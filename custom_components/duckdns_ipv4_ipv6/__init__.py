"""Integrate with DuckDNS IPV4 and IPV6."""
from asyncio import iscoroutinefunction
from datetime import timedelta
import logging

import aiodns
from aiodns.error import DNSError
import voluptuous as vol

import homeassistant.helpers.config_validation as cv
from homeassistant.const import CONF_ACCESS_TOKEN, CONF_DOMAIN
from homeassistant.core import CALLBACK_TYPE, callback
from homeassistant.helpers.aiohttp_client import async_get_clientsession
from homeassistant.helpers.event import async_track_point_in_utc_time
from homeassistant.loader import bind_hass
from homeassistant.util import dt as dt_util


_LOGGER = logging.getLogger(__name__)


DOMAIN = "duckdns_ipv4_ipv6"

CONF_HOSTNAME = "hostname"
CONF_TRY_INDIVIDUAL = "try_individual"
CONF_IPV4_FALLBACK = "ipv4_fallback"
CONF_IPV4_MODE = "ipv4_mode"
CONF_IPV6_MODE = "ipv6_mode"
CONF_IPV4_RESOLVER = "ipv4_resolver"
CONF_IPV6_RESOLVER = "ipv6_resolver"

UPDATE_URL = "https://www.duckdns.org/update"
DEFAULT_HOSTNAME = "myip.opendns.com"
DEFAULT_TRY_INDIVIDUAL = False
DEFAULT_IPV4_FALLBACK = False
DEFAULT_IPV4_MODE = "off"
DEFAULT_IPV6_MODE = "off"
DEFAULT_IPV4_RESOLVER = "208.67.222.222"
DEFAULT_IPV6_RESOLVER = "2620:0:ccc::2"

INTERVAL = timedelta(minutes=5)


SERVICE_UPDATE_IP = "update_ip"

ATTR_HOSTNAME = "hostname"
ATTR_IPV4_FALLBACK = "ipv4_fallback"
ATTR_TRY_INDIVIDUAL = "try_individual"
ATTR_IPV4_MODE = "ipv4_mode"
ATTR_IPV6_MODE = "ipv6_mode"
ATTR_IPV4_RESOLVER = "ipv4_resolver"
ATTR_IPV6_RESOLVER = "ipv6_resolver"
ATTR_IPV4_ADDRESS = "ipv4_address"
ATTR_IPV6_ADDRESS = "ipv6_address"


CONFIG_SCHEMA = vol.Schema(
    {
        DOMAIN: vol.Schema(
            {
                vol.Required(CONF_DOMAIN): cv.string,
                vol.Required(CONF_ACCESS_TOKEN): cv.string,
                vol.Optional(CONF_HOSTNAME, default=DEFAULT_HOSTNAME): cv.string,
                vol.Optional(
                    CONF_TRY_INDIVIDUAL, default=DEFAULT_TRY_INDIVIDUAL
                ): cv.boolean,
                vol.Optional(
                    CONF_IPV4_FALLBACK, default=DEFAULT_IPV4_FALLBACK
                ): cv.boolean,
                vol.Optional(CONF_IPV4_MODE, default=DEFAULT_IPV4_MODE): cv.string,
                vol.Optional(CONF_IPV6_MODE, default=DEFAULT_IPV6_MODE): cv.string,
                vol.Optional(
                    CONF_IPV4_RESOLVER, default=DEFAULT_IPV4_RESOLVER
                ): cv.string,
                vol.Optional(
                    CONF_IPV6_RESOLVER, default=DEFAULT_IPV6_RESOLVER
                ): cv.string,
            }
        )
    },
    extra=vol.ALLOW_EXTRA,
)

SERVICE_UPDATE_IP_SCHEMA = vol.Schema(
    {
        vol.Optional(ATTR_HOSTNAME): vol.Any(None, cv.string),
        vol.Optional(ATTR_TRY_INDIVIDUAL): vol.Any(None, cv.boolean),
        vol.Optional(ATTR_IPV4_FALLBACK): vol.Any(None, cv.boolean),
        vol.Optional(ATTR_IPV4_MODE): vol.Any(None, cv.string),
        vol.Optional(ATTR_IPV6_MODE): vol.Any(None, cv.string),
        vol.Optional(ATTR_IPV4_RESOLVER): vol.Any(None, cv.string),
        vol.Optional(ATTR_IPV6_RESOLVER): vol.Any(None, cv.string),
        vol.Optional(ATTR_IPV4_ADDRESS): vol.Any(None, cv.string),
        vol.Optional(ATTR_IPV6_ADDRESS): vol.Any(None, cv.string),
    }
)


async def async_setup(hass, config):
    """Initialize the DuckDNS component."""
    _LOGGER.info("SETUP - Started component setup")

    domain = config[DOMAIN][CONF_DOMAIN]
    token = config[DOMAIN][CONF_ACCESS_TOKEN]
    hostname = config[DOMAIN][CONF_HOSTNAME]
    try_individual = config[DOMAIN][CONF_TRY_INDIVIDUAL]
    ipv4_fallback = config[DOMAIN][CONF_IPV4_FALLBACK]
    ipv4_mode = config[DOMAIN][CONF_IPV4_MODE]
    ipv6_mode = config[DOMAIN][CONF_IPV6_MODE]
    ipv4_resolver = config[DOMAIN][CONF_IPV4_RESOLVER]
    ipv6_resolver = config[DOMAIN][CONF_IPV6_RESOLVER]

    session = async_get_clientsession(hass)

    async def update_domain_interval(_now):
        """Update the DuckDNS entry."""
        return await _prepare_duckdns_update(
            session,
            domain,
            token,
            hostname,
            ipv4_resolver,
            ipv6_resolver,
            ipv4_mode,
            ipv6_mode,
            ipv4_fallback,
            try_individual,
            ipv4_address=None,
            ipv6_address=None,
        )
        _LOGGER.info("INTERVAL - Update Domain Interval called")

    intervals = (
        INTERVAL,
        timedelta(minutes=1),
        timedelta(minutes=5),
        timedelta(minutes=15),
        timedelta(minutes=30),
    )
    async_track_time_interval_backoff(hass, update_domain_interval, intervals)

    async def update_domain_service(call):
        """Update the DuckDNS IPV6 entry."""

        _LOGGER.info("SERVICE - Service called")

        if "hostname" in call.data:
            if call.data["hostname"]:
                hostname = call.data["hostname"]
            else:
                hostname = None
        else:
            hostname = DEFAULT_HOSTNAME

        if "ipv4_fallback" in call.data:
            if call.data["ipv4_fallback"]:
                ipv4_fallback = call.data["ipv4_fallback"]
            else:
                ipv4_fallback = None
        else:
            ipv4_fallback = DEFAULT_IPV4_FALLBACK

        if "try_individual" in call.data:
            if call.data["try_individual"]:
                try_individual = call.data["try_individual"]
            else:
                try_individual = None
        else:
            try_individual = DEFAULT_TRY_INDIVIDUAL

        if "ipv4_mode" in call.data:
            if call.data["ipv4_mode"]:
                ipv4_mode = call.data["ipv4_mode"]
            else:
                ipv4_mode = None
        else:
            ipv4_mode = DEFAULT_IPV4_MODE

        if "ipv6_mode" in call.data:
            if call.data["ipv6_mode"]:
                ipv6_mode = call.data["ipv6_mode"]
            else:
                ipv6_mode = None
        else:
            ipv6_mode = DEFAULT_IPV6_MODE

        if "ipv4_resolver" in call.data:
            if call.data["ipv4_resolver"]:
                ipv4_resolver = call.data["ipv4_resolver"]
            else:
                ipv4_resolver = None
        else:
            ipv4_resolver = DEFAULT_IPV4_RESOLVER

        if "ipv6_resolver" in call.data:
            if call.data["ipv6_resolver"]:
                ipv6_resolver = call.data["ipv6_resolver"]
            else:
                ipv6_resolver = None
        else:
            ipv6_resolver = DEFAULT_IPV6_RESOLVER

        if "ipv4_address" in call.data:
            if call.data["ipv4_address"]:
                ipv4_address = call.data["ipv4_address"]
            else:
                ipv4_address = None
        else:
            ipv4_address = None

        if "ipv6_address" in call.data:
            if call.data["ipv6_address"]:
                ipv6_address = call.data["ipv6_address"]
            else:
                ipv6_address = None
        else:
            ipv6_address = None

        await _prepare_duckdns_update(
            session,
            domain,
            token,
            hostname,
            ipv4_resolver,
            ipv6_resolver,
            ipv4_mode,
            ipv6_mode,
            ipv4_fallback,
            try_individual,
            ipv4_address,
            ipv6_address,
        )

    hass.services.async_register(
        DOMAIN,
        SERVICE_UPDATE_IP,
        update_domain_service,
        schema=SERVICE_UPDATE_IP_SCHEMA,
    )

    _LOGGER.info("SETUP - Finished component setup")
    return True


async def _get_ip_address(hostname, resolver_address, query_type):
    """Get IP address"""

    _LOGGER.info("GET_ADDRESS - Started IP address resolver")
    response = None

    try:
        resolver = aiodns.DNSResolver()
        resolver.nameservers = [resolver_address]

        try:
            response = await resolver.query(hostname, query_type)
            _LOGGER.info("GET_ADDRESS - Got response from Resolver: %s", response)

        except DNSError as err:
            _LOGGER.warning("GET_ADDRESS - Unable to resolve host: %s", err)

    except:
        _LOGGER.warning(
            'GET_ADDRESS - Unable to setup the Resolver for "resolver" %s',
            resolver_address,
        )

    if response:
        _LOGGER.info("GET_ADDRESS - Got IP address")
        response = response[0].host

    else:
        _LOGGER.warning("GET_ADDRESS - No IP address returned by Resolver")

    _LOGGER.info("GET_ADDRESS - Finished IP address resolver")
    return response


async def _duckdns_update(session, domain, token, ipv4_address=None, ipv6_address=None):
    """Update DuckDNS - If ipv4_address and/or ipv6_address provided, will update it to that/those addresses - If neither addresses are provided, will update only IPV4 using DuckDNS IPV4 autodetect mode."""

    _LOGGER.info("UPDATER - Started")
    params = {"domains": domain, "token": token}

    if ipv4_address:
        params["ip"] = ipv4_address
        _LOGGER.info("UPDATER - IPV4 address provided")

    if ipv6_address:
        params["ipv6"] = ipv6_address
        _LOGGER.info("UPDATER - IPV6 address provided")

    try:
        _LOGGER.info("UPDATER - Performing HTTP GET Request to DuckDNS")

        resp = await session.get(UPDATE_URL, params=params)
        body = await resp.text()

        if body != "OK":
            _LOGGER.warning("UPDATER - Updating DuckDNS domain failed: %s", domain)
            return False

        else:
            _LOGGER.info("UPDATER - Successfully updated DuckDNS domain")

    except:
        _LOGGER.warning("UPDATER - Unable to perform HTTP GET request to DuckDNS")
        return False

    _LOGGER.info("UPDATER - Finished")
    return True


async def _prepare_duckdns_update(
    session,
    domain,
    token,
    hostname,
    ipv4_resolver,
    ipv6_resolver,
    ipv4_mode,
    ipv6_mode,
    ipv4_fallback,
    try_individual,
    ipv4_address=None,
    ipv6_address=None,
):
    """Update DuckDNS"""
    _LOGGER.info("PREPARER - Started")

    success = True

    if ipv4_address:
        _LOGGER.info("IPV4 - IPV4 address provided by the user - %s", ipv4_address)
    else:
        _LOGGER.info("IPV4 - No IPV4 address provided by the user")

        if ipv4_mode == "auto":
            _LOGGER.info("IPV4_MODE = 'auto'")

            response = await _duckdns_update(session, domain, token)

            if not response:
                _LOGGER.warning(
                    "IPV4_AUTODETECT - Unable to update IPV4 address over DuckDNS IPV4 autodetect method"
                )

                if ipv4_fallback:
                    _LOGGER.info(
                        "IPV4_FALLBACK - Attempting to get IPV4 address over NAMESERVER Method"
                    )

                    ipv4_address = await _get_ip_address(hostname, ipv4_resolver, "A")

                    if not ipv4_address:
                        _LOGGER.warning(
                            "IPV4_FALLBACK - Unable to retrieve IPV4 address from NAMESERVER"
                        )
                        success = False

                    else:
                        _LOGGER.info(
                            "IPV4_FALLBACK - IPV4 address successfully retrieved over NAMESERVER Method"
                        )

                else:
                    _LOGGER.warning(
                        "IPV4_FALLBACK - Fallback not triggered, 'ipv4_fallback' not specified or set to 'False'"
                    )
                    success = False

            else:
                _LOGGER.info(
                    "IPV4_AUTODETECT - IPV4 address successfully updated over DuckDNS IPV4 autodetect method"
                )

        elif ipv4_mode == "nameserver":
            _LOGGER.info("IPV4_MODE = 'nameserver'")

            ipv4_address = await _get_ip_address(hostname, ipv4_resolver, "A")

            if not ipv4_address:
                _LOGGER.warning(
                    "IPV4_NAMESERVER - Unable to retrieve IPV4 address from NAMESERVER"
                )

                if ipv4_fallback:
                    _LOGGER.info(
                        "IPV4_FALLBACK - Attempting to update IPV4 address over DuckDNS IPV4 autodetect method"
                    )

                    response = await _duckdns_update(session, domain, token)

                    if not response:
                        _LOGGER.warning(
                            "IPV4_FALLBACK - Unable to update IPV4 address over DuckDNS IPV4 autodetect method"
                        )
                        success = False

                    else:
                        _LOGGER.info(
                            "IPV4_FALLBACK - IPV4 address successfully updated over DuckDNS IPV4 autodetect method"
                        )

                else:
                    _LOGGER.warning(
                        "IPV4_FALLBACK - Fallback not triggered, 'ipv4_fallback' not specified or set to 'False'"
                    )
                    success = False

            else:
                _LOGGER.info(
                    "IPV4_NAMESERVER - IPV4 address successfully retrieved over NAMESERVER Method"
                )

        elif ipv4_mode == "off":
            _LOGGER.info("IPV4 - The 'ipv4_mode' is either 'off' or not specified")

        else:
            _LOGGER.info("IPV4 - The 'ipv4_mode' provided is not a valid option")

    if ipv6_address:
        _LOGGER.info("IPV6 - IPV6 address provided by the user - %s", ipv6_address)
    else:
        _LOGGER.info("IPV6 - No IPV6 address provided by the user")

        if ipv6_mode == "nameserver":
            _LOGGER.info("IPV6_MODE = 'nameserver'")

            ipv6_address = await _get_ip_address(hostname, ipv6_resolver, "AAAA")

            if not ipv6_address:
                _LOGGER.warning(
                    "IPV6_NAMESERVER - Unable to retrieve IPV6 address from NAMESERVER"
                )
                success = False
            else:
                _LOGGER.info(
                    "IPV6_NAMESERVER - IPV6 address successfully retrieved over NAMESERVER Method"
                )

        elif ipv6_mode == "off":
            _LOGGER.info("IPV6 - The 'ipv6_mode' is either 'off' or not specified")

        else:
            _LOGGER.info("IPV6 - The 'ipv6_mode' provided is not a valid option")

    if ipv4_address and ipv6_address:
        _LOGGER.info(
            "PREPARER_IPV4_IPV6 - Updating both IPV4 and IPV6 addresses in the same HTTP GET Request"
        )

        response = await _duckdns_update(
            session, domain, token, ipv4_address=ipv4_address, ipv6_address=ipv6_address
        )

        if response:
            _LOGGER.info(
                "PREPARER_IPV4_IPV6 - Successfully updated both IPV4 and IPV6 addresses in the same HTTP GET Request"
            )
            _LOGGER.info("FINISH Updating DuckDNS ")

            return success

        else:
            _LOGGER.warning(
                "PREPARER_IPV4_IPV6 - Failed to update both IPV4 and IPV6 addresses in the same HTTP GET Request"
            )

            if try_individual:
                _LOGGER.info(
                    "PREPARER_IPV4_IPV6 - Will try to update IPV4 and IPV6 addresses individually"
                )

            else:
                _LOGGER.warning(
                    "PREPARER_IPV4_IPV6 - Will NOT try to update IPV4 and IPV6 addresses individually, 'try_individual' not specified or set to 'False'"
                )
                _LOGGER.info("FINISH Updating DuckDNS ")

                success = False
                return success

    if ipv6_address:
        _LOGGER.info("PREPARER_IPV6 - Updating the IPV6 addresses")

        response = await _duckdns_update(
            session, domain, token, ipv6_address=ipv6_address
        )

        if response:
            _LOGGER.info("PREPARER_IPV6 - Successfully updated IPV6 address")

        else:
            _LOGGER.warning("PREPARER_IPV6 - Unable to update IPV6 address")
            success = False

    if ipv4_address:
        _LOGGER.info("PREPARER_IPV4 - Updating the IPV4 addresses")

        response = await _duckdns_update(
            session, domain, token, ipv4_address=ipv4_address
        )

        if response:
            _LOGGER.info("PREPARER_IPV4 - Successfully updated IPV4 address")

        else:
            _LOGGER.warning("PREPARER_IPV4 - Unable to update IPV4 address")
            success = False

    _LOGGER.info("PREPARER - Finished")
    return success


@callback
@bind_hass
def async_track_time_interval_backoff(hass, action, intervals) -> CALLBACK_TYPE:
    """Add a listener that fires repetitively at every timedelta interval."""
    if not iscoroutinefunction:
        _LOGGER.error("action needs to be a coroutine and return True/False")
        return

    if not isinstance(intervals, (list, tuple)):
        intervals = (intervals,)
    remove = None
    failed = 0

    async def interval_listener(now):
        """Handle elapsed intervals with backoff."""

        nonlocal failed, remove
        try:
            failed += 1
            if await action(now):
                failed = 0
        finally:
            delay = intervals[failed] if failed < len(intervals) else intervals[-1]
            remove = async_track_point_in_utc_time(hass, interval_listener, now + delay)

    hass.async_run_job(interval_listener, dt_util.utcnow())

    def remove_listener():
        """Remove interval listener."""

        if remove:
            remove()  # pylint: disable=not-callable

    return remove_listener
